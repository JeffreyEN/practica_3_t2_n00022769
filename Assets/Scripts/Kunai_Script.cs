using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai_Script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    //public Pj_script pj_Script;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        

        Destroy(this.gameObject, 5);

    }
    void Update()
    {
        if (sr.flipY)
        {
            rb2d.velocity = new Vector2(-15f, rb2d.velocity.y);

        }
        else
        {
            
            rb2d.velocity = new Vector2(15f, rb2d.velocity.y);
        }

    }
}