using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pj_script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    private Transform _transform;

    public float upSpeed = 60f;
    public float RunSpeed = 10f;
    public int vidas = 3;
    private int Puntaje = 0;
    private float C_time = 0f;

    public bool muerto = false;
    private bool EstaTocandoElSuelo = true;
    private bool Escalera = false;

    public Text T_Puntaje;


    public GameObject Kunai_i;
    public GameObject Kunai_d;
    public Zombie_Script __Puntaje;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (muerto == false)
        {

            if (Input.GetKey(KeyCode.RightArrow))
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                setRunAnimation();
                if (Input.GetKey(KeyCode.Z))
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(RunSpeed + 40, rb2d.velocity.y);
                }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
                setRunAnimation();
                if (Input.GetKey(KeyCode.Z))
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(-RunSpeed - 40, rb2d.velocity.y);
                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                setIdleAnimation();
            }
            if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
            if (Input.GetKey(KeyCode.C))
            {
                if (!EstaTocandoElSuelo)
                {
                    setGlideAnimation();
                    rb2d.gravityScale = 10f;
                    rb2d.velocity = Vector2.up * 0;
                }
            }
            else
            {
                rb2d.gravityScale = 20;
                if (!EstaTocandoElSuelo && rb2d.velocity.y < 0)
                {
                    C_time += Time.deltaTime;
                    Caida();
                }

            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                if (sr.flipX)
                {

                    var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Kunai_i, BulletPosition, Quaternion.Euler(0, 0, -90));
                }
                else
                {
                    var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Kunai_d, BulletPosition, Quaternion.Euler(0, 0, -90));
                }

            }
            if (Escalera && (Input.GetKey(KeyCode.UpArrow)))
            {
                rb2d.gravityScale = 0;
                rb2d.velocity = Vector2.up * 10f;
            }
        }
        else
        {
            setJDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            rb2d.gravityScale = 20;

        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
        C_time = 0f;

        if (other.gameObject.layer == 3)
        {
            vidas--;
            Destroy(other.gameObject);
            if (vidas == 0)
                muerto = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 6)
        {
            Escalera = true;
        }
        if (other.gameObject.layer == 7)
        {
            Escalera = false;
        }
    }
    private void Caida()
    {
        if (C_time > 0.33)
            muerto = true;
    }

    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setGlideAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }

    private void setSlideAnimation()
    {
        _animator.SetInteger("Estado", 4);
    }
    private void setJDeadAnimation()
    {
        _animator.SetInteger("Estado", 5);
    }
}
