using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    private Transform _transform;

    public GameObject zombie;
    private float S_time;
    private float Random_N;
    private bool a = false;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();

    }
    void Update()
    {
        Random_N = Random.Range(0,1000);
       // Debug.Log("Rand:" + Random_N);
        S_Random();
        if(a)
            S_time = 0f; a = false;




    }
    void S_Random()
    {
        S_time += Time.deltaTime;
        if (S_time > Random_N - 0.5 && S_time < Random_N)
        {
            var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
            Instantiate(zombie, BulletPosition, Quaternion.identity);
            a = true;
        }
        if (S_time > 5)
            S_time = 0;
                
     //Debug.Log("Time: "+S_time);
     




    }
}
